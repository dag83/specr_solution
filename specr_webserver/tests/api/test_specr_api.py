from src.api.specr_api import app

def test_webserver():
    response = app.test_client().get('/')

    assert response.status_code == 200
