# all the imports
import os
import random
import uuid
from flask import Flask, request, g, redirect, url_for, render_template, flash
import requests
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired, Length

from db import db
from firebase_admin import firestore

# create our little application :)
SECRET_KEY = 'super-secret specr key very special key'
app = Flask(__name__)
app.config.from_object(__name__)


class UploadForm(FlaskForm):
    creator_name = StringField('Your Nickname', validators=[DataRequired(), Length(3, 25)])
    comp_name = StringField('Computer Name', validators=[DataRequired(), Length(3, 50)])

def getDBRef(host):
    if any(x in host for x in ["staging", "localhost", "127.0.0.1"]):
        return db.collection('staging-computers') 
    else:
        return db.collection('computers')


@app.route('/')
def index():
    form = UploadForm()
    return render_template('upload_computer.html', form=form)


@app.route('/show/<comp>')
def show_computer(comp):
    try:
        comp = int(comp, 16)
    except:
        flash('Invalid computer ID (URL) - you get a random instead!', 'warning')
        return redirect(url_for('random_computer'))
    computerDoc_ref = getDBRef(request.host).where('url_id', '==', comp).limit(1).get()
    if not computerDoc_ref:
        flash('Invalid computer ID (URL) - you get a random instead!', 'warning')
        return redirect(url_for('random_computer'))
    specs = computerDoc_ref[0].to_dict()
    return render_template('show_computer.html', computer=specs, comp=hex(int(comp))[2:])

@app.route('/show/10/<comp>')
def dec_computer(comp):
    return redirect(url_for('show_computer', comp=hex(int(comp))[2:]))

@app.route('/show/last')
def last_computer():
    computerDoc_ref = getDBRef(request.host).order_by("url_id", direction=firestore.Query.DESCENDING).limit(1).get()
    comp = computerDoc_ref[0].to_dict()
    max_urlid = int(comp["url_id"])
    return redirect(url_for('show_computer', comp=hex(max_urlid)[2:]))


@app.route('/random')
def random_computer():
    computerDoc_ref = getDBRef(request.host).order_by("url_id", direction=firestore.Query.DESCENDING).limit(1).get()
    comp = computerDoc_ref[0].to_dict()
    max_urlid = int(comp["url_id"])
    rnd_urlid = hex(random.randint(256, max_urlid))[2:]
    return redirect(url_for('show_computer', comp=rnd_urlid))

@app.route('/upload', methods=['GET', 'POST'])
def upload_computer():
    form = UploadForm()
    if form.validate_on_submit():
        file = request.files['file']
    if any(x in request.host for x in ["staging", "localhost", "127.0.0.1"]):
        r = requests.post("https://europe-west1-specr-solution.cloudfunctions.net/staging-parse-dxdiag", data = file)
    else:
        r = requests.post("https://europe-west1-specr-solution.cloudfunctions.net/parse-dxdiag", data = file)
    if r.status_code != 200:
        flash('Could not parse DxDiag.xml file (parse-dxdiag)!', 'error')
        return redirect(url_for('index'))
    parsed_specs = r.json()    
    parsed_specs['system_name'] = form.comp_name.data
    parsed_specs['creator_name'] = form.creator_name.data
    if request.headers.getlist("X-Real-IP"):
        parsed_specs['creator_IP'] = request.headers.getlist("X-Real-IP")[0]
    else:
        parsed_specs['creator_IP'] = request.remote_addr
    computerDoc_ref = getDBRef(request.host).order_by("url_id", direction=firestore.Query.DESCENDING).limit(1).get()
    comp = computerDoc_ref[0].to_dict()
    parsed_specs['url_id'] = int(comp["url_id"]) + 1

    try: 
        getDBRef(request.host).document(str(uuid.uuid4())).set(parsed_specs)
        return redirect(url_for('show_computer', comp=hex(parsed_specs['url_id'])[2:]))
    except Exception as e:
        flash('Database error!', 'error')
        return redirect(url_for('index'))
    

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)
