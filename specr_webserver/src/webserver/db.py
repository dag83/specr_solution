from firebase_admin import firestore, initialize_app, credentials

try:
    cred = credentials.Certificate('key.json')
    default_app = initialize_app(cred)
except:
    default_app = initialize_app()

db = firestore.client()