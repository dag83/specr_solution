import xmltodict

def parse_dxdiag(request):
    if 'file' in request.files:
    	data = request.files['file']
    elif request.data:
        data = request.data
    else:
    	return {"error": "Missing XML file!"}, 400
    try:
    	xml = xmltodict.parse(data)
    except:
    	return {"error": "Invalid XML file!"}, 400

    tmp_did, tmp_mid, tmp_gpu_id = '', '', ''
    opti_words = ['cd', 'CD', 'Cd', 'dvd', 'DVD', 'Dvd', 'virtual', 'Virtual', 'atapi', 'ATAPI', 'DRW', 'CdRom', 'Raid', 'Expansion', 'USB']
    mobo_words = ['System Product Name', 'O.E.M.']
    parsed_specs = {'mobo': '', 'cpus': [], 'gpus': [], 'mem_size': '', 'hdds': [], 'monitors': '', 'os_ver': ''}
    tmp_gpus = []
    tmp_gpu_ids = []
    tmp_cpus = set()
    tmp_monitors = []
    tmp_hdds = set()
    try:
        mobo = xml['DxDiag']['SystemInformation']['SystemModel']
        if not mobo or mobo == ' ':
            mobo = 'Unknown'
        if any(word in mobo for word in mobo_words):
            mobo = "Unknown"
        parsed_specs['mobo'] = mobo
        parsed_specs['os_ver'] = xml['DxDiag']['SystemInformation']['OperatingSystem'].split("(")[0].strip()        
        if "Intel" in xml['DxDiag']['SystemInformation']['Processor']:
            parsed_specs['cpus'].append(' '.join(xml['DxDiag']['SystemInformation']['Processor'].split(',')[0].replace('(R)', '').replace('(TM)', '').replace('(tm)', '').split()))
        else:
            parsed_specs['cpus'].append(' '.join(xml['DxDiag']['SystemInformation']['Processor'].replace('(R)', '').replace('(TM)', '').replace('(tm)', '').replace(', ~', ' @ ').split()))
        parsed_specs['mem_size'] = int(xml['DxDiag']['SystemInformation']['Memory'].replace('MB RAM', '')) / 1024
        try:
            for dd in xml['DxDiag']['DisplayDevices']['DisplayDevice']:
                if "CardName" in dd:
                    if not any(ids in dd["DeviceIdentifier"] for ids in tmp_gpu_ids):
                        tmp_gpus.append(dd['CardName'])
                    tmp_gpu_ids.append(dd["DeviceIdentifier"])
                if "MonitorId" in dd:
                    tmp_monitors.append(dd['MonitorId']) 
            
            for i, gpu in enumerate(tmp_gpus):
                tmp_sli = 0
                for devid in xml['DxDiag']['SystemDevices']['SystemDevice']:
                   if gpu in devid['Name']:
                        tmp_sli += 1
                if tmp_sli > 1:
                    tmp_gpus[i] = tmp_gpus[i] + ' x' + str(tmp_sli) + ' (SLI)'
        except:
            tmp_gpus.append(xml['DxDiag']['DisplayDevices']['DisplayDevice']['CardName'])
            tmp_monitors.append(xml['DxDiag']['DisplayDevices']['DisplayDevice']['MonitorId'])
        if len(tmp_monitors) == 0:
            tmp_monitors.append('No Physical Monitor Detected')
        try:
            for dd in xml['DxDiag']['LogicalDisks']['LogicalDisk']:
                if dd['Model']:
                    if not any(word in dd['Model'] for word in opti_words):
                        tmp_hdds.add(dd['Model'])
        except:
            tmp_hdds.add(xml['DxDiag']['LogicalDisks']['LogicalDisk']['Model'])

        parsed_specs["hdds"] = sorted(list(tmp_hdds))
        parsed_specs["gpus"] = sorted(list(tmp_gpus))
        parsed_specs["monitors"] = sorted(list(tmp_monitors))
    except Exception as e:
        print(e)
        return {"error": "Could not parse dxdiag XML file!"}, 400
    return parsed_specs, 200
