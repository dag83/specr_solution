# Attention!

The file [./src/parse.py](./src/parse.py) runs as a Google Cloud Function available publically from https://europe-west1-specr-solution.cloudfunctions.net/parse-dxdiag

The file [./src/manual_test.py](./src/manual_test.py) is a "quick n dirty" way to test the function locally.

There is no real technical reason why it runs as a Google Cloud Function - honestly I just wanted to experiment with serverless technologies...