import os
import sys
import os
import ast
import copy
from flask import Flask, request

sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../src")
from main import parse_dxdiag

def make_hash(o): # Borrowed from https://stackoverflow.com/a/8714242
  if isinstance(o, (set, tuple, list)):
    return tuple([make_hash(e) for e in o])    
  elif not isinstance(o, dict):
    return hash(o)
  new_o = copy.deepcopy(o)
  for k, v in new_o.items():
    new_o[k] = make_hash(v)
  return hash(tuple(frozenset(sorted(new_o.items()))))

app = Flask(__name__)

@app.route('/upload', methods=['POST'])
def test_parse():
    return parse_dxdiag(request)


with app.test_client() as client:
    with open('./tests/test_dxdiag_files/DxDiag_gaming.xml', 'r') as f:
        xml = f.read()
    rv = client.post('/upload', data=xml)
    r = {"cpus":["Intel Core i7-2600 CPU @ 3.40GHz (8 CPUs)"],"gpus":["NVIDIA GeForce GTX 980"],"hdds":["KINGSTON SH103S3480G","OCZ-OCTANE","SAMSUNG HD501LJ"],"mem_size":32.0,"mobo":"Unknown","monitors":["DEL407E"],"os_ver":"Windows 10 Home 64-bit"}
    assert rv.status_code == 200
    assert make_hash(ast.literal_eval(rv.data.decode("UTF-8"))) == make_hash(r)

    with open('./tests/test_dxdiag_files/DxDiag_dd1.xml', 'r') as f:
        xml = f.read()
    rv = client.post('/upload', data=xml)
    r = {"cpus":["AMD Athlon Neo Processor MV-40 @ 1.6GHz"],"gpus":["ATI Radeon X1200 Series"],"hdds":["WDC WD2500BEVT-60ZCT1 ATA Device"],"mem_size":2.0,"mobo":"HP Pavilion dv2 Notebook PC","monitors":["CMO1233"],"os_ver":"Windows 10 Pro 64-bit"}
    assert rv.status_code == 200
    assert make_hash(ast.literal_eval(rv.data.decode("UTF-8"))) == make_hash(r)

    with open('./tests/test_dxdiag_files/DxDiag_seth(SLI_missing).xml', 'r') as f:
        xml = f.read()
    rv = client.post('/upload', data=xml)
    r = {"cpus":["Intel Core i7-5960X CPU @ 3.00GHz (16 CPUs)"],"gpus":["NVIDIA GeForce GTX 980 Ti x2 (SLI)"],"hdds":["SAMSUNG SSD 830 Series","Samsung SSD 840 EVO 1TB"],"mem_size":16.0,"mobo":"All Series","monitors":["ACR0408","SNYDC02"],"os_ver":"Windows 10 Pro 64-bit"}
    assert rv.status_code == 200
    assert make_hash(ast.literal_eval(rv.data.decode("UTF-8"))) == make_hash(r)

    with open('./tests/test_dxdiag_files/DxDiag_tommy.xml', 'r') as f:
        xml = f.read()
    rv = client.post('/upload', data=xml)
    r = {"cpus":["Intel Core i5-3450 CPU @ 3.10GHz (4 CPUs)"],"gpus":["AMD Radeon R9 200 Series"],"hdds":["OCZ-VERTEX3 ATA Device","ST2000DM001-1CH164 ATA Device"],"mem_size":8.0,"mobo":"Unknown","monitors":["No Physical Monitor Detected"],"os_ver":"Windows 10 Home 64-bit"}
    assert rv.status_code == 200
    assert make_hash(ast.literal_eval(rv.data.decode("UTF-8"))) == make_hash(r)

    exit(0)
