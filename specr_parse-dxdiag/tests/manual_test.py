import xmltodict
from flask import Flask, request, g, redirect, url_for, render_template, flash
from src.main import parse_dxdiag


# configuration
#DATABASE = 'specr.db'
DEBUG = True

# create our little application :)
specr = Flask(__name__)
application = specr
specr.config.from_object(__name__)

@specr.route('/upload', methods=['POST'])
def parse():
    s = parse_dxdiag(request)
    print(s)
    return s

if __name__ == '__main__':
    specr.run(host='0.0.0.0', port='8090')