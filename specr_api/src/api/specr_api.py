# all the imports
import os
import uuid
from flask import Flask, redirect, url_for, jsonify
from flask_restplus import Api, Resource, fields, abort, marshal_with

from db import computerColl_ref


# Classes
class MyApi(Api):
    @property
    def specs_url(self):
        """Monkey patch for HTTPS"""
        scheme = 'http' if '8080' in self.base_url else 'https'
        return url_for(self.endpoint('specs'), _external=True, _scheme=scheme)

# create our little application :)
app = Flask(__name__)
api = MyApi(app)

# Resources
computer_fields = api.model('Computer', {
   "system_name": fields.String(required=True),
   "creator_name": fields.String(required=True),
   "mobo": fields.String(required=True),
   "cpus": fields.List(fields.String),
   "gpus": fields.List(fields.String),
   "mem_size": fields.String(required=True),
   "hdds": fields.List(fields.String),
   "monitors": fields.List(fields.String),
})

@api.route('/computers/<string:id>')
class ComputerResource(Resource):
    @api.marshal_with(computer_fields)
    def get(self, id):
        id = int(id, 16)
        computerDoc_ref = computerColl_ref.where('url_id', '==', id).limit(1).get()
        if not computerDoc_ref:
            abort(404, message="Spec {} doesn't exist".format(id))
        else:
            comp = computerDoc_ref[0].to_dict()
            return comp

# @api.route('/computers')
# class ComputerList(Resource):    
#     @api.marshal_list_with(computer_fields)
#     def get(self):
#         return "List!"
    
    # @api.expect(computer_fields, validate=True)
    # @api.marshal_with(computer_fields)
    # def post(self):
    #     doc_id = str(uuid.uuid4())
    #     comps_ref = computer_ref.document(doc_id).set(api.payload)
    #     return api.payload


# Main
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=int(os.environ.get('PORT', 8080)), debug=True)
