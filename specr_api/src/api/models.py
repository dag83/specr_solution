from sqlalchemy import Column, Integer, String, Text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects import postgresql

Base = declarative_base()

class Computer(Base):
    __tablename__ = 'computers'
    urlid = Column(Integer, primary_key=True)
    system_name = Column(String(100))
    creator_name = Column(String(100))
    creator_ip = Column(postgresql.INET)
    mobo = Column(Text)
    cpu_name = Column(Text)
    gpu_name = Column(Text)
    mem_size = Column(Text)
    hdds = Column(Text)
    monitors = Column(Text)
    os_ver = Column(Text)
