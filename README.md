# specr.me 
This repo is the source code (and CI/CD pipeline) for https://specr.me/.

> specr.me is a website designed to give you a quick and easy way to show off or reference your computer specifications.

https://specr.me/ is a website made by http://dag.ninja/.

https://specr.me/ is hosted on Google Cloud Platform.
